# *
User-agent: *
Allow: /

# AhrefsBot
User-agent: AhrefsBot
Disallow: /

# SemrushBot
User-agent: SemrushBot
Disallow: /

# MJ12bot
User-agent: MJ12bot
Disallow: /

# DotBot
User-agent: DotBot
Disallow: /

# Host
Host: https://yesmore.cc

# Sitemaps
Sitemap: https://yesmore.cc/sitemap.xml
